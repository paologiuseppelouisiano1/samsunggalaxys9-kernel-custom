# samsunggalaxys9+kernel-custom

Archive for compiled S9+ Kernels for SM-G965U1
## Source
## Klabit87
https://github.com/klabit87/android_kernel_samsung_sdm845.git.

## Alpha.0.0.2SecurityUpdateJune-27-2022-CVE-2022-0847 
Fixes:
1. Fixed Dirty Pipe (CVE-2022-0847) Vulnerability 
2. Changed Local Version


