# samsunggalaxys9+kernel-custom

Archive for compiled S9+ Kernels for SM-G965U1

## Source
## mohammad.afaneh
https://github.com/mohammad92/android_kernel_samsung_sdm845

## Alpha.0.0.1SecurityUpdateJune-27-2022-CVE-2022-0847 
Fixes:
1. Fixed Dirty Pipe (CVE-2022-0847) Vulnerability 

## Alpha.0.0.0
Fixes: None 
