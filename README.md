# samsunggalaxys9+kernel-custom

Archive for compiled S9+ Kernels for SM-G965U1

# Custom Kernel

## Alpha.0.0.2SecurityUpdateJune-27-2022-CVE-2022-0847 
Fixes:
1. Fixed Dirty Pipe (CVE-2022-0847) Vulnerability 
2. Changed Local Version


# Stock Kernel

## Alpha.0.0.0SecurityUpdateJune-27-2022-CVE-2022-0847 
Fixes:
1. Fixed Dirty Pipe (CVE-2022-0847) Vulnerability 

